package com.example.tarjetasmoduleapp;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.example.tarjetasmodule.TarjetaFragment;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        TarjetaFragment tarjetaFragment = new TarjetaFragment();

        getFragmentManager().beginTransaction().replace(R.id.fragment_container, tarjetaFragment).commit();
    }
}
